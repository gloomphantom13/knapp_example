#include "knapp_example/order_optimizer.hpp"

struct stat info;

// OrderOptimizer constructor
// Input: folder_path: path to folders containing product and order files
// Checks for validity of path, calls readProducts, initializes the subscribers and publishers
OrderOptimizer::OrderOptimizer(char* folder_path): Node("order_optimizer")
{
  if(stat(folder_path, &info) != 0)
  {
    RCLCPP_ERROR(rclcpp::get_logger("OrderOptimizer"),"Directory '%s' cannot be accessed!", folder_path);
    exit(-1);
  }
  else if(info.st_mode & S_IFDIR)
  {
    directory_ = folder_path;
    readProducts();
  }
  else
  {
    RCLCPP_ERROR(rclcpp::get_logger("OrderOptimizer"), " %s is not a directory", folder_path);
    exit(-1);
  }
  pos_sub_ = this->create_subscription<geometry_msgs::msg::PoseStamped>("currentPosition", 10, std::bind(&OrderOptimizer::position_callback, this, _1));
  order_sub_ = this->create_subscription<knapp_example::msg::Order>("nextOrder", 10, std::bind(&OrderOptimizer::order_callback, this, _1));
  marker_pub_ = this->create_publisher<visualization_msgs::msg::MarkerArray>("order_path", 10);
}

//OrderOptimizer destuctor
OrderOptimizer::~OrderOptimizer()
{
  for(auto p : products_)
  {
    delete p.second;
  }
  products_.clear();
  for(auto o : orders_)
  {
    delete o.second;
  }
  orders_.clear();
}

// readProducts
// Input: -
// Checks for validity of products.yaml, parses products.yaml file, populates
// products_ vector and part_map_
void OrderOptimizer::readProducts()
{
  string line;
  std::ifstream file(directory_ + "/configuration/products.yaml");
  if(file.good())
  {
    Product* current_product;
    string id_string = "- id: ";
    string name_string = "  product: ";
    string part_string = "  - part: ";
    while(getline(file, line))
    {
      if(line.find(id_string) != string::npos)
      {
        line.erase(0, id_string.size());
        int id = std::stoi(line);
        getline(file, line);
        line.erase(0, name_string.size());
        current_product = new Product(id, line);
        products_[id] = current_product;
        getline(file, line);
      }
      else if(line.find(part_string) != string::npos)
      {
        line.erase(0, part_string.size());
        string part_name = line.substr(1, line.size() - 2);
        auto part_iterator = part_map_.find(part_name);
        if(part_iterator == part_map_.end())
        {
          string coordinate_string = "    cx: ";
          getline(file, line);
          double cx = std::stod(line.erase(0, coordinate_string.size()));
          getline(file, line);
          double cy = std::stod(line.erase(0, coordinate_string.size()));
          Part part = {part_name, cx, cy};
          part_map_[part_name] = part;
          current_product->addPart(part);
        }
        else
        {
          current_product->addPart(part_iterator->second);
        }
      }
    }
  }
  else
  {
    RCLCPP_ERROR(rclcpp::get_logger("OrderOptimizer"), "Config file could not be opened!");
    file.close();
    exit(-1);
  }
  file.close();
}

// readOrders
// Input: file_path: path to one of the order files
// Checks for validity of order file, parses order file, creates new Order objects
void OrderOptimizer::readOrders(string file_path)
{
  std::map<int, Order*> orders;
  string line;

  std::ifstream file(file_path);

  if(file.good())
  {
    Order* current_order;
    string id_string = "- order: ";
    string coord_string = "  cx: ";
    string product_string = "  - ";
    while(getline(file, line))
    {
      if(line.find(id_string) != string::npos)
      {
        line.erase(0, id_string.size());
        int id = std::stoi(line);
        getline(file, line);
        double cx = std::stod(line.erase(0, coord_string.size()));
        getline(file, line);
        double cy = std::stod(line.erase(0, coord_string.size()));
        current_order = new Order(id, cx, cy);
        orders[id] = current_order;
        getline(file, line);
      }
      else if(line.find(product_string) != string::npos)
      {
        line.erase(0, product_string.size());
        int product_id = std::stoi(line);
        products_mutex_.lock();
        auto product_iterator = products_.find(product_id);
        products_mutex_.unlock();
        if(product_iterator == products_.end())
        {
          RCLCPP_ERROR(rclcpp::get_logger("OrderOptimizer"), "Product with ID %d not found!", product_id);
        }
        else
        {
          current_order->addProduct(product_iterator->second);
        }
      }
    }
    orders_mutex_.lock();
    orders_.insert(orders.begin(), orders.end());
    orders.clear();
    orders_mutex_.unlock();
  }
  else
  {
    RCLCPP_ERROR(rclcpp::get_logger("OrderOptimizer"), "Order file could not be opened!");
  }
  file.close();
}

// getOrderFiles
// Input: -
// Gets all order file paths from /orders directory and replaces old paths
void OrderOptimizer::getOrderFiles()
{
  order_files_.clear();
  for (const auto & file_name : std::filesystem::directory_iterator(directory_ +  + "/orders"))
  {
    order_files_.push_back(file_name.path().string());
  }
}

// position_callback
// Input: msg: PoseStamped message from "currentPosition" topic
// Saves current robot position into member variable
void OrderOptimizer::position_callback(const geometry_msgs::msg::PoseStamped & msg)
{
  current_pos_.header = msg.header;
  current_pos_.pose = msg.pose;
}

// order_callback
// Input: msg: Order message from "next_order" topic
// Recieves new orders, calls getOrderFiles() to update list off possible orders,
// creates a thread for each file to parse order files, calls executeOrder
void OrderOptimizer::order_callback(const knapp_example::msg::Order & msg)
{
  RCLCPP_INFO(rclcpp::get_logger("OrderOptimizer"),"Recieved new order!");
  getOrderFiles();
  for(auto o : orders_)
  {
    delete o.second;
  }
  orders_.clear();

  std::vector<std::thread> parsingThreads;

  for(uint i = 0; i < order_files_.size(); i++)
  {
    parsingThreads.push_back(std::thread(&OrderOptimizer::readOrders, this, order_files_[i]));
  }
  for(uint i = 0; i < order_files_.size(); i++)
  {
    parsingThreads.at(i).join();
  }

  auto order = orders_.find(msg.order_id);
  if(order == orders_.end())
  {
    RCLCPP_ERROR(rclcpp::get_logger("OrderOptimizer"), "Order with ID %d not found!", msg.order_id);
  }
  else
  {
    executeOrder(msg.order_id, msg.description, order->second);
  }
}

// euclideanDistance
// Input: x1, y1, x2, y2: coordinates of two points
// Calculates the euclidean distance between point (x1/y2) and point (x2/y2)
double OrderOptimizer::euclideanDistance(double x1, double y1, double x2, double y2)
{
    return std::sqrt(std::pow((x2 - x1), 2) + std::pow((y2 - y1), 2));
}

// recursivePath
// Input: cost: distance for current path; in_path: path traversed so far;out_path: nodes to visit
// Recursively calculates all possible distances for all possible combination
// of nodes
void OrderOptimizer::recursivePath(double cost, std::vector<Part> in_path, std::vector<Part> out_path)
{
  if(in_path.empty())
  {
    path_map_[cost] = out_path;
    return;
  }
  else
  {
    for(Part part : in_path)
    {
      std::vector<Part> temp_set(in_path.begin(), in_path.end());
      temp_set.erase(std::remove(temp_set.begin(), temp_set.end(), part), temp_set.end());
      double robot_x = out_path.back().cx_;
      double robot_y = out_path.back().cy_;
      std::vector<Part> new_out(out_path.begin(), out_path.end());
      new_out.push_back(part);
      recursivePath(cost + euclideanDistance(robot_x, robot_y, part.cx_, part.cy_), temp_set, new_out);
    }
    return;
  }
}

// calculateDistances
// Input: used_parts: set of all needed parts in order; robot_x, robot_y: x and y
// coordinate of the robot
// Calculates the shortest path from the starting position to visit each required
// station
std::vector<Part> OrderOptimizer::calculateDistances(std::set<Part> used_parts, float robot_x, float robot_y)
{
  path_map_.clear();
  std::vector<Part> part_order;
  for(Part part : used_parts)
  { 
    std::vector<Part> temp_set(used_parts.begin(), used_parts.end());
    temp_set.erase(std::remove(temp_set.begin(), temp_set.end(), part), temp_set.end());
    std::vector<Part> out_path;
    out_path.push_back(part);
    recursivePath(euclideanDistance(robot_x, robot_y, part.cx_, part.cy_), temp_set, out_path);
  }

  return path_map_.begin()->second;
}

// executeOrder
// Input: order_id, description: ID and description of the order recieved from
// "next_order" topic; order: Order object with corresponding oder ID
// Prints order of operations and publishes corresponding markers
void OrderOptimizer::executeOrder(int order_id, string description, Order* order)
{
  RCLCPP_INFO(rclcpp::get_logger("OrderOptimizer"), "Working on order %d ( %s )", 
              order_id, description.c_str());
  std::vector<string> output;
  float robot_x = current_pos_.pose.position.x;
  float robot_y = current_pos_.pose.position.y;
  auto marker_array = visualization_msgs::msg::MarkerArray();
  auto robot_pos_marker = visualization_msgs::msg::Marker();
  robot_pos_marker.ns = "Robot Position";
  robot_pos_marker.type = 1;
  robot_pos_marker.pose = current_pos_.pose;
  marker_array.markers.push_back(robot_pos_marker);

  std::set<Part> used_parts;

  for(Product* product : order->getProducts())
  {
    for(Part part : product->getParts())
    {
      used_parts.insert(part);
    }
  }

  std::vector<Part> ordered_parts = calculateDistances(used_parts, robot_x, robot_y);

  for(Part part : ordered_parts)
  {
    auto part_marker = visualization_msgs::msg::Marker();
    part_marker.ns = part.name_;
    part_marker.type = 3;
    part_marker.pose.position.x = part.cx_;
    part_marker.pose.position.y = part.cy_;
    marker_array.markers.push_back(part_marker);
    for(Product* product : order->getProducts())
    {
      std::vector<string> out_string = product->printFetching(part);
      output.insert(output.end(), out_string.begin(), out_string.end());
    }
  }

  for(uint i = 0; i < output.size(); i++)
  {
    RCLCPP_INFO(rclcpp::get_logger("OrderOptimizer"), "%d. %s", i+1, output.at(i).c_str());
  }
  RCLCPP_INFO(rclcpp::get_logger("OrderOptimizer"), "%lu. Delivering to destination x: %f, y: %f", output.size()+1, order->getX(), order->getY());
  marker_pub_->publish(marker_array);
}