#include "knapp_example/Order.hpp"

// Order constructor
// Input: id: id of order; cx, cy: coordinates of order destination
Order::Order(int id, double cx, double cy)
{
  id_ = id;
  cx_ = cx;
  cy_ = cy;
}

// addProduct
// Input: product: a product Object
// Adds the product to the vector of products for this order
void Order::addProduct(Product* product)
{
  products_.push_back(product);
}

// getProducts
// Input: -
// Returns the vector containing all products for this order
std::vector<Product*> Order::getProducts()
{
  return products_;
}

// getX
// Input: -
// Returns x coordinate of the order destination
double Order::getX()
{
  return cx_;
}

// getY
// Input: -
// Returns y coordinate of the order destination
double Order::getY()
{
  return cy_;
}