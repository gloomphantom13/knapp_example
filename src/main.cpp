#include "knapp_example/order_optimizer.hpp"


// main
// Input: console arguments; path to folder containing product and order files is required
// Checks for console arguments, starts node
int main(int argc, char *argv[])
{
  if(argc != 2)
  {
    RCLCPP_ERROR(rclcpp::get_logger("OrderOptimizer"), "Wrong number of arguments!");
    return -1;
  }
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<OrderOptimizer>(argv[1]));
  rclcpp::shutdown();
  return 0;
}