#include "knapp_example/Product.hpp"

// Product constructor
// Input: id: id of product; name: name of product
Product::Product(int id, std::string name)
{
  id_ = id;
  name_ = name;
}

// getId()
// Input: -
// Returns ID of product
int Product::getId()
{
  return id_;
}

// getName
// Input: -
// Returns name of product
std::string Product::getName()
{
  return name_;
}

// getParts
// Input: -
// Returns vector of parts belonging to the product
std::vector<Part> Product::getParts()
{
  return parts_;
}

// addPart
// Input: part: a part object
// Adds a part to the part vector
void Product::addPart(Part part)
{
  parts_.push_back(part);
}

// printProduct
// Input: -
// Prints product properties as found in the products.yaml file (for debugging)
void Product::printProduct()
{
  std::cout << std::setprecision(10);
  std::cout << "- id: " << id_ << std::endl;
  std::cout << "  product: " << name_ << std::endl;
  std::cout << "  parts:" << std::endl;
  for(auto & part : parts_)
  {
    std::cout << "  - part: " << part.name_ << std::endl;
    std::cout << "    cx: " << part.cx_ << std::endl;
    std::cout << "    cy: " << part.cy_ << std::endl;
  }
}

// printFetching
// Input: part: a pat object
// Checks if part is required for this product, if true, builds string for
// printing
std::vector<std::string> Product::printFetching(Part part)
{
  std::vector<std::string> out;
  for(auto p : parts_)
  {
    if(p.name_ == part.name_)
    {
      std::string line = "Fetching part '" + part.name_ + "' for product " + 
                         std::to_string(id_) + "' at x: " + std::to_string(part.cy_) + 
                         ", y: " + std::to_string(part.cx_);
      out.push_back(line);
    }
  }
  return out;
}