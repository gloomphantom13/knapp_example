#ifndef PRODUCT_HPP
#define PRODUCT_HPP

#include <string>
#include <vector>
#include <iostream>
#include <iomanip>

struct Part
{
  std::string name_;
  double cx_;
  double cy_;
};

inline bool operator<(const Part &a, const Part &b)
{
  return a.name_ < b.name_;
}

inline bool operator==(const Part &a, const Part &b)
{
  return a.name_ == b.name_;
}

class Product
{
  public:
    Product(int id, std::string name);
    int getId();
    std::string getName();
    std::vector<Part> getParts();
    void addPart(Part part);
    void printProduct();
    std::vector<std::string> printFetching(Part part);
  private:
    int id_;
    std::string name_;
    std::vector<Part> parts_;
};

#endif