#ifndef ORDER_HPP
#define ORDER_HPP

#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include "knapp_example/Product.hpp"

class Order
{
  public:
    Order(int id, double cx, double cy);
    int getId();
    void addProduct(Product* product);
    std::vector<Product*> getProducts();
    double getX();
    double getY();
  private:
    int id_;
    double cx_;
    double cy_;
    std::vector<Product*> products_;
};

#endif