#ifndef ORDER_OPTIMIZER_HPP_INCLUDED
#define ORDER_OPTIMIZER_HPP_INCLUDED

#include "rclcpp/rclcpp.hpp"
#include "knapp_example/Order.hpp"
#include <geometry_msgs/msg/pose_stamped.hpp>
#include <visualization_msgs/msg/marker_array.hpp>
#include "knapp_example/msg/order.hpp"
#include <sys/types.h>
#include <sys/stat.h>
#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <iterator>
#include <filesystem>
#include <thread>
#include <mutex>
#include <set>
#include <cmath>

using std::string;
using std::placeholders::_1;

class OrderOptimizer : public rclcpp::Node
{
  public:
    OrderOptimizer(char* folder_path);
    ~OrderOptimizer();
    void readProducts();
    void readOrders(string file_path);
    void getOrderFiles();
    void executeOrder(int order_id, string description, Order* order);
    std::vector<Part> calculateDistances(std::set<Part> used_parts, float robot_x, float robot_y);
    double euclideanDistance(double x1, double y1, double x2, double y2);
    void recursivePath(double cost, std::vector<Part> in_path, std::vector<Part> out_path);

  private:
    string directory_;
    std::map<string, Part> part_map_;
    std::map<int, Product*> products_;
    std::map<int, Order*> orders_;
    std::vector<string> order_files_;
    geometry_msgs::msg::PoseStamped current_pos_;
    std::mutex orders_mutex_;
    std::mutex products_mutex_;
    std::map<double, std::vector<Part>> path_map_;

    rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr pos_sub_;
    rclcpp::Subscription<knapp_example::msg::Order>::SharedPtr order_sub_;
    rclcpp::Publisher<visualization_msgs::msg::MarkerArray>::SharedPtr marker_pub_;
    void position_callback(const geometry_msgs::msg::PoseStamped & msg);
    void order_callback(const knapp_example::msg::Order & msg);
};

#endif