#include <gtest/gtest.h>
#include "knapp_example/order_optimizer.hpp"

class TestOrderOptimizer : public ::testing::Test
{
  protected:
    void SetUp() override
    {
      rclcpp::init(0, nullptr);
      std::filesystem::path rel_path = "/external_files";
      auto path = std::filesystem::current_path().parent_path().parent_path();
      path += rel_path;
      optimizer_node_ = std::make_shared<OrderOptimizer>(path.u8string().data());
    }
    void TearDown() override
    {
      rclcpp::shutdown();
    }
    std::shared_ptr<OrderOptimizer> optimizer_node_;
    rclcpp::Subscription<visualization_msgs::msg::MarkerArray>::SharedPtr subscriber_;
};


TEST_F(TestOrderOptimizer, part_comparison)
{
  Part part1 = {"Part B", 0, 0};
  Part part2 = {"Part C", 15, 36};
  ASSERT_FALSE(part2 < part1);
  ASSERT_TRUE(part1 < part2);
}

TEST_F(TestOrderOptimizer, euclidean_distance)
{
  double distance = optimizer_node_->euclideanDistance(0, 10, 0, 15);
  ASSERT_NEAR(distance, 5.0, 0.001);
  distance = optimizer_node_->euclideanDistance(153, 329, 763, 423);
  ASSERT_NEAR(distance, 617.2, 0.001);
}

TEST_F(TestOrderOptimizer, part_distance_sorting)
{
  Part part1 = {"Part A", 15, 10};
  Part part2 = {"Part B", 49, 49};
  Part part3 = {"Part C", 30, 35};
  Part part4 = {"Part D", 0, 40};
  std::vector<Part> comp_vec {part1, part4, part3, part2};
  std::set<Part> to_sort{part1, part2, part3, part4};
  auto part_order = optimizer_node_->calculateDistances(to_sort, 25, 25);
  ASSERT_EQ(comp_vec, part_order);
  part_order = optimizer_node_->calculateDistances(to_sort, 50, 50);
  ASSERT_NE(comp_vec, part_order);
}

TEST_F(TestOrderOptimizer, simple_order)
{
  visualization_msgs::msg::MarkerArray markers;
  bool markers_posted = false;

  auto node = std::make_shared<rclcpp::Node>("marker_subscriber_node");
  subscriber_ = node->create_subscription<visualization_msgs::msg::MarkerArray>("order_path", 10, 
    [&markers, &markers_posted](const visualization_msgs::msg::MarkerArray::SharedPtr msg)
    {
      markers = *msg;
      markers_posted = true;
    });

  Part part1 = {"Part A", 791.86304, 732.23236};
  Part part2 = {"Part B", 550.09924, 655.423};
  Part part3 = {"Part C", 281.39413, 68.39627};
  Product* prod1 = new Product(933, "Product 933");
  prod1->addPart(part3);
  prod1->addPart(part1);
  prod1->addPart(part2);
  Product* prod2 = new Product(604, "Product 604");
  prod2->addPart(part1);
  prod2->addPart(part2);
  prod2->addPart(part2);
  prod2->addPart(part3);
  Product* prod3 = new Product(216, "Product 216");
  prod3->addPart(part2);
  prod3->addPart(part3);
  prod3->addPart(part1);
  prod3->addPart(part2);
  Order* order = new Order(1000009, 237.5396, 454.80585);
  order->addProduct(prod1);
  order->addProduct(prod2);
  order->addProduct(prod3);

  optimizer_node_->executeOrder(1000009, "This is a test order", order);

  rclcpp::executors::MultiThreadedExecutor executor;
  executor.add_node(node);  

  auto duration = std::chrono::seconds(2); 
  auto timer = node->create_wall_timer(duration, [&executor]()
  {
      executor.cancel();
  });
  
  executor.spin();

  delete prod1;
  delete prod2;
  delete prod3;
  delete order;
  ASSERT_TRUE(markers_posted);
  ASSERT_EQ(markers.markers[0].ns, "Robot Position");
  ASSERT_EQ(markers.markers[0].type, 1);
  ASSERT_EQ(markers.markers[1].type, 3);
  ASSERT_EQ(markers.markers[2].type, 3);
  ASSERT_EQ(markers.markers[3].type, 3);
}


int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  
  return RUN_ALL_TESTS();
}